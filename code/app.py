####################################################################
#                                                                  #
#   Mastermind App                                                 #
#   Author: Adam Savel                                             #
#   Date Created: 10/07/21                                         #
#   Date Modified: 10/13/21                                        #
#                                                                  #
#   This project is to be a recreation of the 'Master Mind' game   #
#   in which two players participate. One player selects a 'code'  #
#   from a set of colored pegs and conceals it from the other. The #
#   other player then has a set amount of attempts to guess the    #
#   chosen code. With every guess, the player who chose the code   #
#   tells the player how many pegs are the correct color and how   #
#   many are in the correct positions without giving away which    #
#   pegs that may be.                                              #
#                                                                  #
#   This implementation of the game will allow the user to select  #
#   options such as number of colors, length of password, number   #
#   of attemps. The player will play against the computer with the #
#   computer as code maker and the player as the guesser.          #
#                                                                  #
####################################################################

# This project will assist me in learning 'PySimpleGui'
import PySimpleGUI as psg

from random import randint
from math import ceil
from time import sleep

# Game Flow:
# - Main Menu with options selection
# - Press Start to begin
# - Code is generated and hidden from player
# - Game window displays board
# - Player can make a guess
# - Computer will compare guess to answer and provide feedback
# - If the player wins or loses, a splash screen will show
# - Return to main menu

# Set theme
psg.theme = psg.OFFICIAL_PYSIMPLEGUI_THEME

# Initialize game options
colorCnt = None
passLength = None
attemptCnt = None

# Create window object
window = None

# Playtime variables
password = []   # Generated password to be guessed
attempts = []   # Player attempts
feedback = []   # Feedback from attempts
curGuess = []   # Current player guess
currentAttempt = 0  # Current attempt number
liveAttempts = []   # List of TK Graph objects for displaying attempts
liveFeedback = []   # List of TK Graph objects for displaying feedback

# Combobox options
colorCntOptions = [2, 3, 4, 5, 6, 7, 8, 9, 10]
passLengthOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
guessCntOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

# Possible Colors
colorMap = (
    'red',
    'green',
    'blue',
    'purple',
    'cyan',
    'yellow',
    'orange',
    'pink',
    'brown',
    'white',
    'black'
)

def makeMainMenu():

    layout = [
        [psg.Column([[psg.Text('MasterMind', font='Any 25')]], justification='c')],
        [
            psg.Text('# of Colors:'),
            psg.Combo(key='COLORCNT', values=colorCntOptions, default_value=6, readonly=True),
            psg.Text('Length:'),
            psg.Combo(key='PASSLENGTH', values=passLengthOptions, default_value=4, readonly=True),
            psg.Text('Guesses:'),
            psg.Combo(key='ATTEMPTCNT', values=guessCntOptions, default_value=12, readonly=True)
        ],
        [psg.Column([[psg.Button('Start', key='BTNSTART')]], justification='c')]
    ]

    return psg.Window('MasterMind', layout)

def makeGameWindow():
    
    # Determine the size of the game board
    width = (passLength + ceil(float(passLength) / 4.0)) * 100
    height = attemptCnt * 100

    # Generate button panel
    btnPanel = []
    for a in range(passLength):
        btnPanel.append(psg.Button(key = 'peg' + str(a), size = (4, 2), button_color = ('white', 'grey')))
    btnPanel.append(psg.Button(button_text='Submit', key='BTNSUB'))
    
    # Assemble the layout
    layout = [[psg.Column([[psg.Text('MasterMind', font='Any 25')]], justification='c')]]
    layout += [[psg.Column([[psg.Graph((width / 2, height / 2), (0, height), (width, 0), key='board')]], justification='c')]]
    layout += [[psg.Column([btnPanel], justification='c')]]

    # Draw the icons onto the game board graph, leaving an ID with which they can be modified.

    # Init the window
    gameWindow = psg.Window('MasterMind', layout, finalize=True)

    # Grab the graph from the initialized board
    graph = gameWindow['board']

    # Generate the board
    for row in range(attemptCnt):
        liveAttempts.append([])
        liveFeedback.append([])
        for col in range(passLength):
            xPos = col * 100 + 50
            yPos = row * 100 + 50
            liveAttempts[row] += [graph.draw_circle((xPos, yPos), 48, fill_color='gray')]

            if passLength / 2 > col:
                xPos2 = (passLength * 100) + (col * 50) + 25
                yPos2 = (row * 100) + 25
                liveFeedback[row] += [graph.draw_circle((xPos2, yPos2), 24, fill_color='gray')]
            else:
                xPos2 = (passLength * 100) + ((col - passLength / 2) * 50) + 25
                yPos2 = (row * 100) + 75
                liveFeedback[row] += [graph.draw_circle((xPos2, yPos2), 24, fill_color='gray')]

    return gameWindow

def pickColor(pos):
    rowLen = 5

    grid = [[ColorButton(colorMap[c + j * rowLen]) for c in range(0, rowLen)] for j in range(0, colorCnt // rowLen)]
    grid += [[ColorButton(colorMap[c + colorCnt - colorCnt % rowLen]) for c in range(0, colorCnt % rowLen)]]

    layout = [[psg.Text('Pick a color', font='Def 18')]] + grid

    window = psg.Window('Window Title', layout, no_titlebar=True, grab_anywhere=True, keep_on_top=True, use_ttk_buttons=True, location = pos)
    
    color_chosen = None
    while True:  # Event Loop
        event, values = window.read()
        if event == psg.WIN_CLOSED:
            color_chosen = None
            break
        elif event in colorMap:
            color_chosen = event
            break
    window.close()

    return color_chosen

def ColorButton(color):
    """
    A User Defined Element - returns a Button that configured in a certain way.
    :param color: Tuple[str, str] ( color name, hex string)
    :return: sg.Button object
    """
    return psg.B(button_color=('white', color), pad=(0, 0), size=(1, 1), key=color, tooltip=f'{color[0]}:{color[1]}', border_width=0)

def initValues():
    # Clear the playtime values.
    global password # Generated password to be guessed
    global attempts # Player attempts
    global feedback # Feedback from attempts
    global currentAttempt   # Current attempt number
    global liveAttempts     # List of TK Graph objects for displaying attempts
    global liveFeedback     # List of TK Graph objects for displaying feedback

    password = []
    attempts = []
    feedback = []
    currentAttempt = 0
    liveAttempts = []
    liveFeedback = []

    clearCurGuess()
    for peg in range(passLength):
        password.append(colorMap[randint(0, colorCnt - 1)])

def clearCurGuess():
    global curGuess
    curGuess = []
    for i in range(passLength):
        curGuess.append(None)

# Create Window
window = makeMainMenu()

# Application Loop
while True:
    # Read the window events
    event, values = window.read()
    sleep(0.01)

    # Event handler
    # Events:
    # 0: Window Closed
    # 1: Start Button
    # 2: Peg Selection Button
    # 3: Submit Button

    # 0: Window Closed
    if event == psg.WIN_CLOSED:
        # Exit Application Loop
        break
    # 1: Start Button
    elif event == 'BTNSTART':
        # Get the game option values
        colorCnt = window['COLORCNT'].get()
        passLength = window['PASSLENGTH'].get()
        attemptCnt = window['ATTEMPTCNT'].get()

        # Clear the playtime values
        initValues()
        print(password)
        # Close window and open game window
        window.close()
        window = makeGameWindow()
    # 2: Peg Selection Button
    elif 'peg' in event:
        # Open popup color selector
        coords = list(window.CurrentLocation())
        coords[0] += int(event[3]) * 50
        coords[1] += window.Size[1] - 50
        window.Disable()
        chosenColor = pickColor(coords)
        window.Enable()
        window.force_focus()
        window[event].update(button_color=('white', chosenColor))

        # Save selection into guess list
        curGuess[int(event[3])] = chosenColor
    # 3: Submit Button
    elif event == 'BTNSUB':
        # Check if input is valid
        if None in curGuess:
            continue

        # Submit the guess to the board
        attempts.append(curGuess)
        for peg in range(passLength):
            window['board'].TKCanvas.itemconfig(liveAttempts[currentAttempt][peg], fill=curGuess[peg])
            window['peg' + str(peg)].update(button_color=('white', 'gray'))

        # Check game end conditions
        if password == curGuess:
            psg.popup_no_titlebar('You Win!')
            window.close()
            window = makeMainMenu()
            continue
        elif currentAttempt == attemptCnt - 1:
            psg.popup_no_titlebar('You Lose :(')
            window.close()
            window = makeMainMenu()
            continue

        # Give Feedback
        blackCnt = 0
        whiteCnt = 0
        tempPass = password.copy()

        for peg in range(passLength - 1, -1, -1):
            if curGuess[peg] == tempPass[peg]:
                blackCnt += 1
                tempPass.pop(peg)
                curGuess.pop(peg)
            
        for peg in range(len(curGuess) - 1, -1, -1):
            if curGuess[peg] in tempPass:
                whiteCnt +=1
                tempPass.remove(curGuess[peg])

        for feed in range(blackCnt):
            window['board'].TKCanvas.itemconfig(liveFeedback[currentAttempt][feed], fill='black')

        for feed in range(whiteCnt):
            window['board'].TKCanvas.itemconfig(liveFeedback[currentAttempt][feed + blackCnt], fill='white')

        # Clear the guess field
        clearCurGuess()

        # Increase the attempt count
        currentAttempt += 1

window.close()