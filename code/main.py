import tkinter as tk
from tkinter import ttk
from enum import Enum
from random import randint

class Color(Enum):
    BLACK = 1
    WHITE = 2
    RED = 3
    GREEN = 4
    BLUE = 5
    YELLOW = 6
    CYAN = 7
    PURPLE = 8
    PINK = 9
    ORANGE = 10

# Main Application Code

class Master_Mind(tk.Tk):

    # Load default values into the class variables
    codeSize = 4
    colorCount = 5
    guessNum = 10
    secretCode = []

    def __init__(self, *args, **kwargs):

        tk.Tk.__init__(self, *args, **kwargs)

        self.frames = {}

        for F in (Menu, Game):
            frame = F(self.container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, stick="nsew")

        self.show_frame(Menu)

    def configure_gui(self):
        self.title("Master Mind")
        self.geometry("500x720")
        self.resizable(False, False)

        self.container = tk.Frame(self)
        self.container.pack(side="top", fill="both", expand = True)
        self.container.grid_rowconfigure(0, weight=1)
        self.container.grid_columnconfigure(0, weight=1)

    def show_frame(self, container):
        frame = self.frames[container]
        frame.tkraise()

# 

class Menu(tk.Frame):
    
    def __init__(self, parent, controller):

        # Generate main menu layout.
        tk.Frame.__init__(self, parent, bg="white")

        testLabel = tk.Label(self, text="Welcome to Master Mind!")
        testLabel.pack()

        numList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]

        self.selectionFrame = tk.Frame(self)
        self.selectionFrame.pack()

        lblCodeSize = tk.Label(self.selectionFrame, text="Code Size")
        lblCodeSize.grid(row = 1, column = 0)

        self.cbCodeSize = ttk.Combobox(self.selectionFrame, values = numList, state = "readonly")
        self.cbCodeSize.set(str(codeSize))
        self.cbCodeSize.grid(row = 2, column = 0)

        lblColorCount = tk.Label(self.selectionFrame, text="Color Count")
        lblColorCount.grid(row = 1, column = 1)

        self.cbColorCount = ttk.Combobox(self.selectionFrame, values = numList, state = "readonly")
        self.cbColorCount.set(str(colorCount))
        self.cbColorCount.grid(row = 2, column = 1)

        lblGuessCount = tk.Label(self.selectionFrame, text="Guess Count")
        lblGuessCount.grid(row = 1, column = 2)

        self.cbGuessCount = ttk.Combobox(self.selectionFrame, values = numList, state = "readonly")
        self.cbGuessCount.set(str(guessNum))
        self.cbGuessCount.grid(row = 2, column = 2)

        btnSubmit = tk.Button(self, text="Start", command = lambda: self.clkSubmit(controller))
        btnSubmit.pack()

    def clkSubmit(self, controller):

        # Set the game values.
        codeSize = int(self.cbCodeSize.get())
        colorCount = int(self.cbColorCount.get())
        guessNum = int(self.cbGuessCount.get())

        # Generate a random code.
        secretCode = self.genCode()

        # Load game screen.
        controller.show_frame(Game)

    def genCode(self):

        # Generate a random set of numbers to be used as the 'secret code'.
        arr = []
        for num in range(codeSize):
            arr = arr + [randint(1, colorCount)]
        return arr

class Game(tk.Frame):
    
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        displayWidth = 100
        displayHeight = 100

        # Generate display.
        mainCanvas = tk.Canvas(self, width = displayWidth, height = displayHeight)
        mainCanvas.pack()

        # Generate input area.
        userInput = tk.Frame(self, width = 100, height = 100)
        userInput.pack()

if __name__ == "__main__":
    game = Master_Mind()
    game.mainloop()